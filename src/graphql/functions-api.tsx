import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Query = {
  __typename?: 'Query';
  echo?: Maybe<Response>;
};


export type QueryEchoArgs = {
  toEcho: Scalars['String'];
};

export type Response = {
  __typename?: 'Response';
  contentType?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
};

export type EchoQueryVariables = Exact<{
  toEcho: Scalars['String'];
}>;


export type EchoQuery = (
  { __typename?: 'Query' }
  & { echo?: Maybe<(
    { __typename?: 'Response' }
    & Pick<Response, 'contentType' | 'body'>
  )> }
);


export const EchoDocument = gql`
    query echo($toEcho: String!) {
  echo(toEcho: $toEcho) {
    contentType
    body
  }
}
    `;

/**
 * __useEchoQuery__
 *
 * To run a query within a React component, call `useEchoQuery` and pass it any options that fit your needs.
 * When your component renders, `useEchoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEchoQuery({
 *   variables: {
 *      toEcho: // value for 'toEcho'
 *   },
 * });
 */
export function useEchoQuery(baseOptions: Apollo.QueryHookOptions<EchoQuery, EchoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<EchoQuery, EchoQueryVariables>(EchoDocument, options);
      }
export function useEchoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<EchoQuery, EchoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<EchoQuery, EchoQueryVariables>(EchoDocument, options);
        }
export type EchoQueryHookResult = ReturnType<typeof useEchoQuery>;
export type EchoLazyQueryHookResult = ReturnType<typeof useEchoLazyQuery>;
export type EchoQueryResult = Apollo.QueryResult<EchoQuery, EchoQueryVariables>;