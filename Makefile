# VERSION defines the project version for the bundle.
# Update this value when you upgrade the version of your project.
# To re-generate a bundle for another specific version without changing the standard setup, you can:
# - use the VERSION as arg of the bundle target (e.g make bundle VERSION=0.0.2)
# - use environment variables to overwrite this value (e.g export VERSION=0.0.2)
VERSION ?= latest

# IMAGE_TAG_BASE defines the docker.io namespace and part of the image name for remote images.
# This variable is used to construct full image tags for bundle and catalog images.
#
# For example, running 'make bundle-build bundle-push catalog-build catalog-push' will build and push both
# graphql-gateway.cluster.local/graphql-gateway-operator-bundle:$VERSION and graphql-gateway.cluster.local/graphql-gateway-operator-catalog:$VERSION.
IMAGE_TAG_BASE ?= registry.gitlab.com/faasaf/petshop

# Image URL to use all building/pushing image targets
IMG ?= $(IMAGE_TAG_BASE):$(VERSION)

OPENFAAS_GATEWAY_SVC ?= http://gateway.faas.svc.cluster.local:8080/function
OPENFAAS_GATEWAY ?= http://localhost:31112
CLUSTER_TARGET ?= microk8s

all: docker-build

define faas-cli
OPENFAAS_GATEWAY_SVC=$(OPENFAAS_GATEWAY_SVC) \
IMAGE_REGISTRY=$(IMAGE_TAG_BASE) \
VERSION=$(VERSION) \
$(FAAS_CLI)
endef

##@ General

# The help target prints out all targets with their descriptions organized
# beneath their categories. The categories are represented by '##@' and the
# target descriptions by '##'. The awk commands is responsible for reading the
# entire set of makefiles included in this invocation, looking for lines of the
# file as xyz: ## something, and then pretty-format the target and help. Then,
# if there's a line with ##@ something, that gets pretty-printed as a category.
# More info on the usage of ANSI control characters for terminal formatting:
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
# More info on the awk command:
# http://linuxcommand.org/lc3_adv_awk.php

help: ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Build

docker-build: ## Build docker image with the manager.
	docker build -t ${IMG} .

docker-push: ## Push docker image with the manager.
	docker push ${IMG}

##@ Deployment

faas-pull: faas-cli
	cd functions; $(faas-cli) template pull

build-resources: kustomize
	cd ./resources/overlays/$(CLUSTER_TARGET) && $(KUSTOMIZE) edit set image petshop=${IMG}
	$(KUSTOMIZE) build ./resources/overlays/$(CLUSTER_TARGET)

deploy: kustomize ## Deploy controller to the K8s cluster specified in ~/.kube/config.
	cd ./resources/overlays/$(CLUSTER_TARGET) && $(KUSTOMIZE) edit set image petshop=${IMG}
	$(KUSTOMIZE) build ./resources/overlays/$(CLUSTER_TARGET) | kubectl apply -f -

undeploy: kustomize ## Undeploy controller from the K8s cluster specified in ~/.kube/config.
	cd ./resources/overlays/$(CLUSTER_TARGET) && $(KUSTOMIZE) edit set image petshop=${IMG}
	$(KUSTOMIZE) build ./resources/overlays/$(CLUSTER_TARGET) | kubectl delete -f -

build-functions: faas-cli ## Build functions
	cd functions; $(faas-cli) build --envsubst -f ./stack.yml

push-functions: faas-cli ## Push functions
	cd functions; $(faas-cli) push --envsubst -f ./stack.yml

build-function-%: faas-cli ## Build specific function
	cd functions; $(faas-cli) build --envsubst -f ./stack.yml --filter "$*"

push-function-%: faas-cli ## Push specific function
	cd functions; $(faas-cli) push --envsubst -f ./stack.yml --filter "$*"

deploy-functions: faas-cli
	cd functions; $(faas-cli) deploy --gateway $(OPENFAAS_GATEWAY) --envsubst -f ./stack.yml

deploy-function-%: faas-cli
	cd functions; $(faas-cli) deploy --gateway $(OPENFAAS_GATEWAY) --envsubst -f ./stack.yml --filter "$*"

OS := $(shell uname -s | tr '[:upper:]' '[:lower:]')
ARCH := $(shell uname -m | sed 's/x86_64/amd64/')

.PHONY: kustomize
KUSTOMIZE = $(shell pwd)/bin/kustomize
kustomize: ## Download kustomize locally if necessary.
ifeq (,$(wildcard $(KUSTOMIZE)))
ifeq (,$(shell which kustomize 2>/dev/null))
	@{ \
	set -e ;\
	mkdir -p $(dir $(KUSTOMIZE)) ;\
	curl -sSLo - https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v3.8.7/kustomize_v3.8.7_$(OS)_$(ARCH).tar.gz | \
	tar xzf - -C bin/ ;\
	}
else
KUSTOMIZE = $(shell which kustomize)
endif
endif

.PHONY: faas-cli
FAAS_CLI = $(shell pwd)/bin/faas-cli
faas-cli: ## Download faas-cli locally if nesessary
ifeq (,$(wilcard $(FAAS_CLI)))
ifeq (,$(shell which faas-cli 2>/dev/null))
	@{ \
	set -e ;\
	mkdir -p $(dir $(FAAS_CLI)) ;\
	cat ./get.sh | BINLOCATION=$(dir $(FAAS_CLI)) sh -a; \
	}
else
FAAS_CLI = $(shell which faas-cli)
endif
endif
