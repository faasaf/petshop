# Petshop

This is an example application built on top of faasaf.
The code demonstrates how to create functins, map them to a GraphQl Endpoint and generate, code from the result graphql schema.

## Getting Started

This will assume that you have Docker installed and available via `/var/run/docker.sock`
To get started we will be using a local installation of microk8s.
The easiest way is to install it via `snap` or [checkout out here](https://microk8s.io/docs) a more detailed instruction.

Please follow [faas: Usage with docker](https://gitlab.com/faasaf/faasaf/-/blob/main/readme.md#usage-with-docker) and come back after you have installed `faasaf`.

This will get your local cluster up and running with Openfaas and installs the [openfaas-graphql-gateway-operator](https://gitlab.com/faasaf/openfaas-graphql-gateway-operator) and the [graphql-gateway-operator](https://gitlab.com/faasaf/graphql-gateway-operator).

Also edit your `/etc/hosts` to have `petshop.local` point to the IP address of metallb.

```bash
echo "192.168.178.99  petshop.local" | sudo tee -a /etc/hosts
```

### Just deploy without development

```bash
git clone git@gitlab.com:faasaf/petshop.git
cd petshop
make deploy-functions deploy

```

Open up your application on `https://petshop.local`.
You can query the graphql endpoint via `https://petshop.local/graphql`.

### Build and deploy Petshop to tinker around

```bash
git clone git@gitlab.com:faasaf/petshop.git
cd petshop
## Build and push functions
make IMAGE_TAG_BASE=localhost:32000 build-functions push-functions
## Compile Application
yarn install
yarn build
## Build and push docker images
make IMAGE_TAG_BASE=localhost:32000/petshop docker-build docker-push
## Deploy Functions
make IMAGE_TAG_BASE=localhost:32000 deploy-functions
## Deploy Application
make IMAGE_TAG_BASE=localhost:32000/petshop deploy
```

Open up your application on `https://petshop.local`.
You can query the graphql endpoint via `https://petshop.local/graphql`.

Try add more functions and update the graphql code via `yarn gengql` after the new functions have been deployed with the correct labels and annotations.
