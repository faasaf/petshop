FROM node:alpine

RUN npm install -g nodemon

ADD ./build /var/www
WORKDIR /var/www

RUN yarn add serve
CMD ["/usr/local/bin/yarn", "serve"]
